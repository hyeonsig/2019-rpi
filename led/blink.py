import RPi.GPIO as GPIO
import time 

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)
led = 18
GPIO.setup(led, GPIO.OUT)

for var in range(1, 6):
    GPIO.output(led, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(led, GPIO.LOW)
    time.sleep(1)

print("Application terminated.")

