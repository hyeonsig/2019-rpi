import RPi.GPIO as GPIO
import time 

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)
led = 18
switch = 4

GPIO.setup(led, GPIO.OUT)
GPIO.setup(switch, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#GPIO.setup(switch, GPIO.IN)

GPIO.output(led, False)
try:
    while True:
        GPIO.wait_for_edge(switch, GPIO.FALLING)
        GPIO.output(led, not GPIO.input(led))
        time.sleep(0.1)

except KeyboardInterrupt:
    GPIO.output(led, False)
    GPIO.cleanup()
    print("Application terminated.")

