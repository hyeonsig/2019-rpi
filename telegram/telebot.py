import datetime
from time import sleep
import telepot
from telepot.loop import MessageLoop
import RPi.GPIO as GPIO
import Adafruit_DHT

sensor = Adafruit_DHT.DHT11

led_pin = 18
dht_pin = 2

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(led_pin, GPIO.OUT)

now = datetime.datetime.now()

def handle(msg):
  chat_id = msg['chat']['id']  # Receiving the message from telegram
  command = msg['text']  # Getting text from the message

  show_keyboard = {'keyboard': [['/on', '/off'], ['/temp', '/humid']]}
  print('Received:')
  print(command)

  # Comparing the incoming message to send a reply according to it
  if command == '/hi':
    bot.sendMessage(chat_id, str("Hi! userName"), reply_markup=show_keyboard)
  elif command == '/time':
    bot.sendMessage(
      chat_id, str("Time: ") + str(now.hour) + str(":") + str(now.minute) + str(":") + str(now.second))
  elif command == '/date':
    bot.sendMessage(
      chat_id, str("Date: ") + str(now.day) + str("/") + str(now.month) + str("/") + str(now.year))
  elif command == '/on':
    bot.sendMessage(chat_id, str("Led is ON"))
    GPIO.output(led_pin, True)
  elif command == '/humid':
    result1 = Adafruit_DHT.read_retry(sensor, dht_pin)
    bot.sendMessage(chat_id, str(result1[0]))
  elif command == '/temp':
    result2 = Adafruit_DHT.read_retry(sensor, dht_pin)
    bot.sendMessage(chat_id, str(result2[1]))
  elif command == '/off':
    bot.sendMessage(chat_id, str("Led is OFF"))
    GPIO.output(led_pin, False)


bot = telepot.Bot('766995092:AAG2wRmRJEj2r8s28V8BlHEbthKIeLc-h3I')
print (bot.getMe())

MessageLoop(bot, handle).run_as_thread()
print ('Listening....')

while (True):
  sleep(10)
