import time
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO

MQTT_HOST = "maqiatto.com"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
MQTT_TOPIC = ""
MQTT_MSG = "ON"

button_on_pin = 
button_off_pin = 

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(button_on_pin, GPIO.IN)
GPIO.setup(button_off_pin, GPIO.IN)

timestamp_on = time.time()
timestamp_off = time.time()

def on_publish(client, userdata, mid):
    print "Message Published..."

def on_button_callback(channel):
    global timestamp_on
    timenow = time.time()

    if (timenow - timestamp_on) >= 0.3:
        print("On Button was pushed!")
        client.publish(MQTT_TOPIC, "ON")
    timestamp_on = timenow

def off_button_callback(channel):
    global timestamp_off
    timenow = time.time()

    if(timenow - timestamp_off) >= 0.3:
        print("Off Button was pushed!")
        client.publish(MQTT_TOPIC, "OFF")
    timestamp_off = timenow

# Initiate MQTT Client
client = mqtt.Client()

# Register publish callback function
client.on_publish = on_publish
GPIO.add_event_detect(button_on_pin,GPIO.RISING,callback=on_button_callback)
GPIO.add_event_detect(button_off_pin,GPIO.RISING,callback=off_button_callback)

# Connect with MQTT Broker
client.username_pw_set("", "")
client.connect(MQTT_HOST, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)

client.loop_forever()

client.disconnect()
GPIO.cleanup()
