import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO

MQTT_HOST = "maqiatto.com"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
MQTT_TOPIC = "hyeonsig@wisoft.io/harang"

led_pin = 18

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(led_pin, GPIO.OUT)

def on_message(client, userdata, message):
    result = str(message.payload.decode("utf-8"))
    print("received message = ", str(message.payload.decode("utf-8")))

    if (result.upper() == "ON"):
        GPIO.output(led_pin, True)
    elif (result.upper() == "OFF"):
        GPIO.output(led_pin, False)
    else:
        print("Illegal Argument Exception!")

# Initiate MQTT Client
client = mqtt.Client()

# Register received message callback function
client.on_message = on_message

# Connect with MQTT Broker
client.username_pw_set("hyeonsig@wisoft.io", "harang")
client.connect(MQTT_HOST, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)

client.subscribe(MQTT_TOPIC)

# Loop from MQTT_Broker
client.loop_forever()
