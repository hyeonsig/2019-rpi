import Adafruit_DHT
import time

sensor = Adafruit_DHT.DHT11
dht_pin = 2


while True:
    humidity, temperature = Adafruit_DHT.read_retry(sensor, dht_pin)

    if humidity is not None and temperature is not None:
        print("Temp={}*C Humidity={}%".format(temperature, humidity))
    else:
        print("Failed to get reading.")

    time.sleep(2) 
